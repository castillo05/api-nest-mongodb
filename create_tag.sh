#!/bin/bash
git config --global user.email "developer@gmail.com";
git config --global user.name "Continuous Integration";
git tag -d $(git tag -l)
if ! git remote | grep "tag-origin" -q; then
  git remote add tag-origin "https://oauth2:${CI_BOT_ACCESS_TOKEN}@gitlab.com/${CI_PROJECT_PATH}.git"
fi
git tag "${VERSION}" -m "release ${VERSION}"
git push tag-origin "${VERSION}"
git tag -d $(git tag -l)