#!/bin/bash

if git ls-remote --tags --refs origin | sed 's/.*\///' | grep "${VERSION}" -q; then
  echo "El tag \e[31m${VERSION}\e[0m ya existe en este repositorio, por favor cambie el numero de versión en gradle.properties";
  exit 1;
else
  if git tag -l | grep "${VERSION}" -q; then
    git tag -d "${VERSION}"
  fi
  echo "\e[95m Versión para tag ${VERSION} OK!!!\e[0m";
fi