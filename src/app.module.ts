import { Module, HttpModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { NewsModule } from './apis/news/news.module';
import { ScheduleModule } from '@nestjs/schedule';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import NewsServiceService from './news-service/news-service.service';
import { NewsSchema } from './apis/news/schema/news.schema';
@Module({
  imports: [
    NewsModule,
    HttpModule.register({
      timeout: 5000,
      maxRedirects: 5,
    }),
    MongooseModule.forFeatureAsync([
      {
        name: 'News',
        useFactory: () => {
          return NewsSchema;
        },
      },
    ]),
    ScheduleModule.forRoot(),
    ConfigModule.forRoot({ envFilePath: ['.env.development'], isGlobal: true }),
    MongooseModule.forRoot(
      `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}${process.env.DB_URI}`,
    ),
  ],
  controllers: [AppController],
  providers: [AppService, NewsServiceService],
})
export class AppModule {}
