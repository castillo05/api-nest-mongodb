import { HttpModule, Module } from '@nestjs/common';
import NewsService from './news.service';
import NewsController from './news.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { NewsSchema } from './schema/news.schema';

@Module({
  imports: [
    HttpModule.register({
      timeout: 5000,
      maxRedirects: 5,
    }),
    MongooseModule.forFeatureAsync([
      {
        name: 'News',
        useFactory: () => {
          return NewsSchema;
        },
      },
    ]),
  ],
  controllers: [NewsController],
  providers: [NewsService],
})
export class NewsModule {}
