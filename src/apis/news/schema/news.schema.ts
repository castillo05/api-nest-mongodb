import * as mongoose from 'mongoose';
export const NewsSchema = new mongoose.Schema(
  {
    title: { type: String, required: true },
    author: { type: String, required: true },
    created_at_i: { type: String, required: true },
    story_url: { type: String, default: '' },
    parent_id: { type: Number, required: true },
    isDelete: { type: Boolean, default: false },
  },
  {
    timestamps: true,
  },
);
