import { Test, TestingModule } from '@nestjs/testing';
import NewsController from './news.controller';

// TODO: add test cases
import NewsService from './news.service';
describe('News Api', () => {
  let service: NewsService;
  let controller: NewsController;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NewsService],
      controllers: [NewsController],
    }).compile();
    service = module.get<NewsService>(NewsService);
    controller = module.get<NewsController>(NewsController);
  });
  it('should return news', async () => {
    expect(await controller.getNews()).toBe('Hello World New Service!');
  });
});
