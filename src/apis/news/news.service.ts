import { HttpException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
@Injectable()
export default class NewsService {
  constructor(@InjectModel('News') private readonly model: Model<any>) {}
  getNews(): string {
    return 'Hello World New Service!';
  }

  async findAll(): Promise<any[]> {
    const data = await this.model
      .find({
        isDelete: false,
      })
      .sort({ created_at_i: -1 });
    return data;
  }

  async deleteNews(id: string): Promise<any> {
    try {
      const searchNews = await this.model.findById(id);
      if (searchNews) {
        const data = await this.model.updateOne(
          { _id: id },
          { $set: { isDelete: true } },
        );
        return data;
      } else {
        throw new HttpException('Not Found', 404);
      }
    } catch (error) {
      return error;
    }
  }
}
