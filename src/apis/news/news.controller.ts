import { Controller, Delete, Get, HttpException, Param } from '@nestjs/common';
import NewsService from './news.service';

@Controller('news')
export default class NewsController {
  constructor(private readonly newsService: NewsService) {}
  @Get()
  getNews() {
    return this.newsService.findAll();
  }

  @Delete('/:id')
  async deleteNews(@Param('id') id: string) {
    const res = await this.newsService.deleteNews(id);
    if (res.modifiedCount > 0) {
      return {
        status: 200,
        message: 'Delete Success',
      };
    } else {
      throw new HttpException('Not Found', 404);
    }
  }
}
