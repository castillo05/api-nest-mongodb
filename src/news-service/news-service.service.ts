import { HttpException, HttpService, Injectable } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { Cron } from '@nestjs/schedule';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
@Injectable()
export default class NewsServiceService {
  constructor(
    private readonly http: HttpService,
    @InjectModel('News') private readonly model: Model<any>,
  ) {
    this.findAll();
  }

  @Cron('0 0 */1 * * *')
  async findAll(): Promise<AxiosResponse[]> {
    const response = await this.http
      .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .toPromise()
      .catch((err) => {
        throw new HttpException(err.response.data, err.response.status);
      });
    const news = response.data.hits;

    const newsToSave = news.map(async (item) => {
      const parent = await this.model.findOne({
        parent_id: item.parent_id,
      });
      if (!parent) {
        if (!item.story_title) {
          item.story_title = item.title;
        }

        const newData = new this.model({
          title: item.story_title,
          story_url: item.story_url,
          created_at_i: item.created_at,
          author: item.author,
          parent_id: item.parent_id,
        });
        newData.save();
      }
    });

    return response.data.hits;
  }
}
